/************************************************
* Student Name: Anna Bloch and Rotem Elem		*
* Exercise Name: Ex 4							*
* File description: server connection header    		*
*************************************************/
#ifndef SERVERCONNECTION_H_
#define SERVERCONNECTION_H_
#include <iostream>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

using namespace std;

class ServerConnection{
public:
	int server_port;
	int sock;

	/***********************************************************
	 * constructor
	************************************************************/
	ServerConnection();
	/***********************************************************
	 * deconstructor
	************************************************************/
	virtual ~ServerConnection();
	/***********************************************************
	 * virtual initialization of server connection
	************************************************************/
	void virtual initialization()=0;
	/***********************************************************
	 * virtual, reading from client and returning recieved bytes as string
	************************************************************/
	string virtual readFromClient()=0;
	/***********************************************************
	 * virtual , sending output to client
	************************************************************/
	void virtual sendToClient(const string& line) =0;
	/***********************************************************
	 * virtual close connection
	************************************************************/
	void virtual closeConnection() =0;


};






#endif /* SERVERCONNECTION_H_ */
