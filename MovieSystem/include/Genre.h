/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Header file for Genre
****************************************/
#ifndef GENRE_H_
#define GENRE_H_
#include <vector>
#include "Movie.h"
#include <string>
#include <iostream>
using namespace std;
class Movie;

class Genre{

	string type;
	vector<Movie*> movies;
public:
/***********************************************************
 * constructor with parameters
************************************************************/
Genre(string genre);
/***********************************************************
 * constructor
************************************************************/
Genre();
/***********************************************************
 * returns genre name
************************************************************/
string getGenre();
/***********************************************************
 * set genre name
************************************************************/
void setGenre(string g);
/***********************************************************
 * destructor
************************************************************/
~Genre();
/***********************************************************
 * prints genre's movie database
************************************************************/
void printMyMovies();
/***********************************************************
 * returns index of movie in genre database
************************************************************/
int searchMovie(string id);
/***********************************************************
 * adds movie object to genre db
************************************************************/
void addMeToMovie(Movie* m);
/***********************************************************
 * deletes movie from genre db
************************************************************/
void deleteMovie(string code);


};





#endif /* GENRE_H_ */
