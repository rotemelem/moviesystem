/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Header file for Actor
****************************************/
#ifndef ACTOR_H_
#define ACTOR_H_
#include "CrewMember.h"
#include <string>
#include <iostream>
using namespace std;

class Actor :public CrewMember {
public:
	/***********************************************************
	 * constructor
	************************************************************/
	Actor();
	/***********************************************************
	 * destructor
	************************************************************/
	~Actor();
	/***********************************************************
	 * prints Actor info
	************************************************************/
	void printMember(); //prints actor's details as requested
};




#endif /* ACTOR_H_ */
