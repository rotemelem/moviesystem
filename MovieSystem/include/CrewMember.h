/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Header file for CrewMember
* which holds all the functions you can make
* on each type of crew members (actor,writer,producer and director
****************************************/

#ifndef CREWMEMBER_H_
#define CREWMEMBER_H_

#include <iostream>
#include <vector>
#include <string>
#include "Movie.h"
using namespace std;
class Movie;
//class Sort;
class CrewMember{

public:
	vector<Movie*> movies;
	int myId;
	string myName , myGender, myJob;
	double myAge;

/***********************************************************
 * constructor
************************************************************/
CrewMember();
/***********************************************************
 * destructor
************************************************************/
virtual ~CrewMember();
/***********************************************************
 * set name
************************************************************/
void setName(string name);
/***********************************************************
 * return name
************************************************************/
string getName();
/***********************************************************
 * set id
************************************************************/
void setId(int id);
/***********************************************************
 * return integer as id
************************************************************/
int getId() const;
/***********************************************************
 * set gender
************************************************************/
void setGender(string g);
/***********************************************************
 * return gender
************************************************************/
string getGender();
/***********************************************************
 * set job
************************************************************/
void setJob(string job);
/***********************************************************
 * return job
************************************************************/
string getJob();
/***********************************************************
 * set age
************************************************************/
void setAge(double age);
/***********************************************************
 * return age
************************************************************/
double getAge() const;
/***********************************************************
 * print object info - virtual function because every object
 * that derives from this class will implement independently
************************************************************/
virtual void printMember();
/***********************************************************
 * add movie object to my movies list
************************************************************/
void addMeToMovie(Movie* m);
/***********************************************************
 * prints movies list
************************************************************/
void printAllMovies();
/***********************************************************
 * deletes itself
************************************************************/
void deleteCrewMember();
/***********************************************************
 * deletes movie m from my movies list
************************************************************/
void deleteMeFromMovie(string id);
/***********************************************************
 * returns true if a crew member has the movie in its database
************************************************************/
bool searchMovie(string id);
/***********************************************************
 * returns the number of movies a crewmember has been a part of
************************************************************/
int amountOfMovies()const;

};




#endif /* CREWMEMBER_H_ */
