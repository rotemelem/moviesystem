/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Header file for Director
****************************************/

#ifndef DIRECTOR_H_
#define DIRECTOR_H_
#include "CrewMember.h"
#include <string>
#include <iostream>
using namespace std;

class Director :public CrewMember {

public:
	/***********************************************************
	 * constructor
	************************************************************/
	Director();
	/***********************************************************
	 * destructor
	************************************************************/
	~Director();
	/***********************************************************
	 * prints info
	************************************************************/
	void printMember();
};




#endif /* DIRECTOR_H_ */
