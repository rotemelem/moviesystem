/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Header file for Producer
****************************************/

#ifndef PRODUCER_H_
#define PRODUCER_H_
#include "CrewMember.h"
#include <string>
#include <iostream>
using namespace std;

class Producer : public CrewMember {
public:
/***********************************************************
 * constructor
************************************************************/
	Producer();
/***********************************************************
 * destructor
************************************************************/
	~Producer();
/***********************************************************
 * prints info
************************************************************/
	void printMember();
};





#endif /* PRODUCER_H_ */
