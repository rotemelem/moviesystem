/************************************************************
* Student Name: Rotem Elem, Anna Bloch			*
* Exercise Name: EX 4										*
* File description: UDP connection*
*************************************************************/
#ifndef SERVERUDP_H_
#define SERVERUDP_H_
#include <iostream>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include "ServerConnection.h"

class ServerUDP : public ServerConnection{
	public:
    struct sockaddr_in from;
    struct sockaddr_in sin;
    int bytes;
    int port;

	/************************************************************
	* constructor												*
	*************************************************************/
    ServerUDP(int uport);
	/************************************************************
	* deconstructor												*
	*************************************************************/
	 virtual ~ServerUDP();
	/*************************************************************
	 * initialize connection type UDP			 *
	 *************************************************************/
	void initialization();
	/*************************************************************
	* recieves data from client			 *
	**************************************************************/
	string readFromClient();
	/*************************************************************
	* sends data 					 *
	**************************************************************/
	void sendToClient(const string& line);
	/**************************************************************
	* closes the connection										  *
	**************************************************************/
    void closeConnection();

};






#endif /* SERVERUDP_H_ */
