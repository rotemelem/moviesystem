/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Header file for Writer
****************************************/
#ifndef WRITER_H_
#define WRITER_H_
#include "CrewMember.h"
#include <string>
#include <iostream>
using namespace std;

class Writer :public CrewMember {
public:
	/***********************************************************
	 * constructor
	************************************************************/
	Writer();
	/***********************************************************
	 * destructor
	************************************************************/
	~Writer();
	/***********************************************************
	 * print info
	************************************************************/
	void printMember();
};





#endif /* WRITER_H_ */
