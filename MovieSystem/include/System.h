/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Header file for System
****************************************/
#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <iostream>
#include <vector>
#include "Movie.h"
#include "CrewMember.h"
#include "Sort.h"
#include <pthread.h>
using namespace std;


class System {

public:
vector<Movie*> movies;
vector <CrewMember*> crews;
vector <Genre> genres;
bool menu,print;
static System* instance;
static pthread_mutex_t lock;

/***********************************************************
 * constructor
************************************************************/
System();
/***********************************************************
 * destructor
************************************************************/
~System();
/***********************************************************
 * initialize lock
************************************************************/
static void openLock();
/***********************************************************
 * function is static so that only one System will be created
************************************************************/
static System* getInstance();
/***********************************************************
 * add new movie
************************************************************/
void newMovie(string code,string name, int length, int year, double rank, string desc);
/***********************************************************
 * gets crew details as string, breaks it down and sends to CrewMember class
************************************************************/
void newCrew(int type,int id,double age,string desc,string gender,string name);
/***********************************************************
 * sends movie and crew as objects to classes to add to lists
************************************************************/
void addCrewToMovie(string code,int id);
/***********************************************************
 * add genre to movie
************************************************************/
void addGenre(string code,string genre);
/***********************************************************
 * gets requested sort type and sends to movie class to sort as requested
************************************************************/
void setSortType(string movieId, int sortType);
/***********************************************************
 * finds crew member and sends object to class to print
************************************************************/
void printCrew(int id);
/***********************************************************
 * finds movie and sends object to class to print
************************************************************/
void printMovie(string id);
/***********************************************************
 * breaks string into ids and combines two movies every time
 *  until all combined, return true if successful
************************************************************/
void combineMovies(string moviesIds);
/***********************************************************
 * finds Crew object and sends to class to print movie list
************************************************************/
void printCrewMovies(int code);
/***********************************************************
 * finds Movie object and sends to class to print all crew members
************************************************************/
void printMoviesCrew(string id);
/***********************************************************
 * finds Movie object and sends to class to print all Genres
************************************************************/
void printMoviesGenres(string id);
/***********************************************************
 * deletes movie from list and object
************************************************************/
void deleteMovie(string id);
/***********************************************************
 * deletes crew from list and object
************************************************************/
void deleteCrew(int id);
/***********************************************************
 * sends crew object to movie list to delete from crew list
************************************************************/
void deleteCrewFromMovie(string code,int id);
/***********************************************************
 * prints movie list
************************************************************/
void printAllMovies();
/***********************************************************
 * print crew list
************************************************************/
void printAllCrews();
/***********************************************************
 * searches for movie in db by id and returns its index in the db
************************************************************/
int searchMovie(string movieId);
/***********************************************************
 * searches for movie in db by id and returns its index in the db
************************************************************/
int searchCrewMember(int crewId);
/***********************************************************
 * prints movies by genre
************************************************************/
void printByGenre(string input);
/***********************************************************
 * returns genre index in system's db
************************************************************/
int searchGenre(string g);
/***********************************************************
 * deletes all system's db before exiting program
************************************************************/
void terminate();

};




#endif /* SYSTEM_H_ */
