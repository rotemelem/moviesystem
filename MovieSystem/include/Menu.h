/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 3
* File description: Header file for Menu
****************************************/

#ifndef MENU_H_
#define MENU_H_
#include "System.h"
#include "ServerConnection.h"
#include <sstream>
#include <string>
#include <iostream>
#include <pthread.h>
using namespace std;

class Menu{
public:
	System *s;
	string code,space;
	int id;
	static pthread_mutex_t lock;
	static bool check;
	static Menu* instance;
/***********************************************************
* constructor
************************************************************/
Menu();
/***********************************************************
* destructor
************************************************************/
~Menu();
/***********************************************************
 * gets user input and sends input to functions
************************************************************/
void start(ServerConnection* c, System* s);
/***********************************************************
 * calls to new movie
************************************************************/
void newMovie(stringstream& line);
/***********************************************************
 * breaks input and calls to new crew
************************************************************/
void newCrew(stringstream& line);
/***********************************************************
 * breaks input and calls to add crew
************************************************************/
void addCrewToMovie(stringstream& input);
/***********************************************************
 * add genre to movie
************************************************************/
void addGenre(stringstream& in);
/***********************************************************
 * gets requested sort type and sends to movie class to sort as requested
************************************************************/
void setSortType(stringstream& in);
/***********************************************************
 * finds crew member and sends object to class to print
************************************************************/
void printCrew(stringstream& in);
/***********************************************************
 * finds movie and sends object to class to print
************************************************************/
void printMovie(stringstream& in);
/***********************************************************
 * breaks string into ids and combines two movies every time
 *  until all combined, return true if successful
************************************************************/
void combineMovies(stringstream& in);
/***********************************************************
 * finds Crew object and sends to class to print movie list
************************************************************/
void printCrewMovies(stringstream& in);
/***********************************************************
 * finds Movie object and sends to class to print all crew members
************************************************************/
void printMoviesCrew(stringstream& in);
/***********************************************************
 * deletes movie from list and object
************************************************************/
void deleteMovie(stringstream& in);
/***********************************************************
 * deletes crew from list and object
************************************************************/
void deleteCrew(stringstream& in);
/***********************************************************
 * sends crew object to movie list to delete from crew list
************************************************************/
void deleteCrewFromMovie(stringstream& in);
/***********************************************************
 * prints movie list
************************************************************/
void printAllMovies();
/***********************************************************
 * print crew list
************************************************************/
void printAllCrews();
/***********************************************************
 * prints movies by genre
************************************************************/
void printByGenre(stringstream& in);
/***********************************************************
 * function implements singelton in order to only
 * give access to one object.
************************************************************/
static Movie* getInstance();
/***********************************************************
 * deletes all database and terminate program
************************************************************/
void endProgram();

};



#endif /* MENU_H_ */
