/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Header file for Movie
* Movie handles all the function you can make on a Movie
* in the system
****************************************/
#ifndef MOVIE_H_
#define MOVIE_H_

#include <vector>
#include <string>
#include <iostream>
#include "Genre.h"
#include "CrewMember.h"
using namespace std;
class CrewMember;
class Genre;


class Movie {

	string myName,mySummary, myCode;
	int myYear,srtType;
	double myLength, myRank;
	vector <Genre> genre;
	vector <CrewMember*> crewmembers;

public:

	Movie();//constructor
	~Movie();//destructor
	void setName(string name);
	string getName();
	void setDate(int date);
	int getDate();
	void getGenre();
	void setCode(string code);
	string getCode();
	void setLength(double length);
	double getLength();
	void setRank(double rank);
	double getRank();
void setSummary(string summary);
string getSummary();
void setSort(int sort);
int getSort();
/***********************************************************
 * add new crew to crew list
************************************************************/
bool addCrew(CrewMember* c);
/***********************************************************
 * add new genre to movie
************************************************************/
bool addGenre (Genre g);
/***********************************************************
 * sort by specific type and print
************************************************************/
void sort(vector<CrewMember*> c);
/***********************************************************
 * print crewmembers list
************************************************************/
void printCrew();
/***********************************************************
 * print movie details
************************************************************/
void printMovie();
/***********************************************************
 * Receives another movie and combines the two and returns new movie
************************************************************/
void combineMovies(Movie* m);
/***********************************************************
 * delete itself
************************************************************/
void deleteMe();
/***********************************************************
 * delete crew member from list & returns true if successful
************************************************************/
void deleteCrew(CrewMember* c);
/***********************************************************
 * returns true if crew is in movie's database
************************************************************/
bool searchCrew(int id);
/***********************************************************
 * returns movie's genre data base
************************************************************/
vector<Genre> returnGenre();
/***********************************************************
 * returns true if movie belongs to gnere
************************************************************/
bool searchGenre(string g);
/***********************************************************
 * returns movie's crew member data base
************************************************************/
vector<CrewMember*> getCrewList();
};



#endif /* MOVIE_H_ */
