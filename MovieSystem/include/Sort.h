#ifndef SORT_H_
#define SORT_H_
#include "CrewMember.h"
#include <iostream>
using namespace std;

/***********************************************************
 * operator overloading - sorts by ID
************************************************************/
struct byId{
	 bool operator () (const CrewMember* c1, const CrewMember* c2){
		return c1->getId() <= c2->getId();
	}
};
/***********************************************************
 * operator overloading - sorts by age
************************************************************/
struct byAge{
	bool operator () (const CrewMember* c1, const CrewMember* c2){
		return c1->getAge() >= c2->getAge();
	}
};
/***********************************************************
 * operator overloading - sorts by crew member's amount of movies
************************************************************/
struct byAmountOfMovies{
	bool operator () (const CrewMember* c1, const CrewMember* c2){
		return c1->amountOfMovies() >= c2->amountOfMovies();
	}
};

#endif /* SORT_H_ */
