/************************************************
* Student Name: Anna Bloch and Rotem Elem		*
* Exercise Name: Ex 4							*
* File description: server TCP header   		*
*************************************************/
#ifndef SERVERTCP_H_
#define SERVERTCP_H_


#include <iostream>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include "ServerConnection.h"

using namespace std;


class ServerTCP: public ServerConnection {
public:
	int client_sock;
	int read_bytes;
	int port;

	/***********************************************************
	 * constructor
	************************************************************/
	ServerTCP(int port);
	ServerTCP(const ServerTCP* s);
	/***********************************************************
	 * deconstructor
	************************************************************/
	~ServerTCP();
	/***********************************************************
	 * opens server connection and starts listening
	************************************************************/
	void initialization();
	/***********************************************************
	 * reads bytes from client and returning them as string
	************************************************************/
	string readFromClient();
	/***********************************************************
	 * sending output to client
	************************************************************/
	void sendToClient(const string& line);
	/************************************************************
	*creating socket for each client's thread and reading data							*
	*************************************************************/
	ServerTCP* communicate();
	/***********************************************************
	 * close connection
	************************************************************/
	void closeConnection();
};




#endif /* SERVERTCP_H_ */
