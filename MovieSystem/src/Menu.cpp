/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 3
* File description: Menu gets users input and
* by input calls to functions
****************************************/

#include "Menu.h"
#include "ServerConnection.h"
#include "ServerTCP.h"
#include <sstream>
#include <string>
#include <iostream>
using namespace std;


/***********************************************************
 * constructor
************************************************************/
Menu::Menu(){

}
Menu::~Menu(){

}
/***********************************************************
 * loops while code != -1, gets user's input, break input,
 * by getting the first argument of input, decides what function
 * to call, inside each function, breaks the input to arguments
 * and calls to System.
 * when user enter -1, loop ends and we call to the end function
 * to delete db and exit
************************************************************/
void Menu::start(ServerConnection* c, System* sys){

	s = sys;
	stringstream buffer;
	streambuf * old = std::cout.rdbuf(buffer.rdbuf());

	int code =0;
	while(code != -1){
	//getline(cin,line);
	string line = c->readFromClient();
	stringstream in;
	in<<line;
	in>>code;
	switch(code){
	case 1: {newMovie(in);break;}
	case 2: {newCrew(in);break;}
	case 3: {addCrewToMovie(in); break;}
	case 4: {addGenre(in); break;}
	case 5: {setSortType(in); break;}
	case 6: {printMoviesCrew(in); break;}
	case 7: {printMovie(in); break;}
	case 8: {combineMovies(in); break;}
	case 9: {printCrewMovies(in); break;}
	case 10: {deleteMovie(in); break;}
	case 11: {deleteCrew(in); break; }
	case 12: {deleteCrewFromMovie(in); break;}
	case 13: {printAllMovies(); break;}
	case 14: {printAllCrews(); break;}
	case 15: {printByGenre(in); break;}
	}

	if(code != -1){
		string send = buffer.str();
		c->sendToClient(send);
		buffer.str(std::string());
		buffer.clear();
	}


	}

	endProgram();
}
/***********************************************************
 * breaks input and calls to new movie
************************************************************/
void Menu::newMovie(stringstream& in){
	string desc,name;
	int length, year;
	double rank;
	getline(in,space,' ');
	getline(in,code,' ');
	getline(in,name,' ');
	in>>length>>year>>rank;
	getline(in,space,' ');
	getline(in,desc);
	s->newMovie(code,name,length,year,rank,desc);

}
/***********************************************************
 * breaks input and calls to new crew
************************************************************/
void Menu::newCrew(stringstream& in){
	int type,id;
		double age;
		string desc,gender,name,space;
		getline(in,space,' ');
		in>>type>>id>>age;
		getline(in,space,' ');
		getline(in,desc,' ');
		getline(in,gender,' ');
		getline(in,name);
		s->newCrew(type,id,age,desc,gender,name);

}
/***********************************************************
 * breaks input and calls to add crew to movie
************************************************************/
void Menu::addCrewToMovie(stringstream& in){
	getline(in,space,' ');
	getline(in,code,' ');
	in>>id;
	s->addCrewToMovie(code,id);
}
/***********************************************************
 * breaks input and calls to add genre
************************************************************/
void Menu::addGenre(stringstream& in){
		string g;
		getline(in,space,' ');
		getline(in,code,' ');
		getline(in,g);
		s->addGenre(code,g);
}
/***********************************************************
 * breaks input and calls to set sort type
************************************************************/
void Menu::setSortType(stringstream& in){
	getline(in,space,' ');
	getline(in,code,' ');
	int type;
	in>>type;
	s->setSortType(code,type);
}
/***********************************************************
 * breaks input and calls to print movie's crew
************************************************************/
void Menu::printMoviesCrew(stringstream& in){
	getline(in,space,' ');
	getline(in,code,' ');
	s->printMoviesCrew(code);

}
/***********************************************************
 * breaks input and calls to print movie
************************************************************/
void Menu::printMovie(stringstream& in){
	getline(in,space,' ');
	getline(in,code);
	s->printMovie(code);
}
/***********************************************************
 * breaks input and calls to combine movies
************************************************************/
void Menu::combineMovies(stringstream& in){
	getline(in,space,' ');
	getline(in,code);
	s->combineMovies(code);
}
/***********************************************************
 * breaks input and calls to print crew's movie
************************************************************/
void Menu::printCrewMovies(stringstream& in){
	getline(in,space,' ');
	in>>id;
	s->printCrewMovies(id);
}
/***********************************************************
 * breaks input and calls to delete movie
************************************************************/
void Menu::deleteMovie(stringstream& in){
	getline(in,space,' ');
	getline(in,code);
	s->deleteMovie(code);
}
/***********************************************************
 * breaks input and calls to delete crew
************************************************************/
void Menu::deleteCrew(stringstream& in){
	getline(in,space,' ');
	in>>id;
	s->deleteCrew(id);
}
/***********************************************************
 * breaks input and calls to delete crew from movie
************************************************************/
void Menu::deleteCrewFromMovie(stringstream& in){
	getline(in,space,' ');
	getline(in,code,' ');
	in>>id;
	s->deleteCrewFromMovie(code,id);
}
/***********************************************************
 * breaks input and calls to print all movies
************************************************************/
void Menu::printAllMovies(){
	s->printAllMovies();
}
/***********************************************************
 * breaks input and calls to print all crews
************************************************************/
void Menu::printAllCrews(){
	s->printAllCrews();
}
/***********************************************************
 * breaks input and calls to print by genre
************************************************************/
void Menu::printByGenre(stringstream& in){
	getline(in,space,' ');
	in>>code;
	s->printByGenre(code);
}
/***********************************************************
 * calls to terminate in system, and deletes system object
************************************************************/
void Menu::endProgram(){

	delete s;


}
