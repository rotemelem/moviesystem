/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Header file for System
****************************************/
#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include "Movie.h"
#include "System.h"
#include "CrewMember.h"
#include "Actor.h"
#include "Director.h"
#include "Producer.h"
#include "Writer.h"
#include "Genre.h"
#include "stdio.h"
#include <algorithm>
using namespace std;

pthread_mutex_t System::lock;
System* System::instance = NULL;
System* System::getInstance(){
	if(!instance){

		pthread_mutex_lock(&lock);
		if(!instance){
		instance = new System;

		}
	pthread_mutex_unlock(&lock);
	}
	return instance;
}
/********************************
 *initialize lock			*
*********************************/
void System::openLock(){

	if (pthread_mutex_init(&lock,NULL)!=0){
		cout<<"lock initialization failed"<<endl;
				}
}
System::System(){
	//constructor
	menu = true;
	print = true;
}
System::~System(){
	vector<CrewMember*>::iterator j;
	for(j = crews.begin();j!=crews.end();j++){
		delete (*j);
	}
	vector<Movie*>::iterator i;
	for(i = movies.begin(); i!=movies.end();i++){
		delete (*i);
	}


}

/***********************************************************
 * creates new Movie object.
 * sets all of movies info that was recived through input
 * checks if input is valid
 * if valid pushes movie into the movies data base
************************************************************/
void System::newMovie(string code,string name, int length, int year, double rank, string desc){
	Movie* m = new Movie;
	int index = searchMovie(code);
	if(year <0 || length <=0 ||index != -1)
	{
		cout << "Failure"<<endl;
		return;
	}
	m->setCode(code);
	m->setDate(year);
	m->setLength(length);
	m->setName(name);
	m->setRank(rank);
	m->setSummary(desc);
	movies.push_back(m);
	cout<<"Success"<<endl;

}
/***********************************************************
 * creates new Crewmember object.
 * checks if input is valid , if so goes on to set crew member info
 * according to input's type, creates the crew member
 * sets its info by input and pushes into crew members db
************************************************************/
void System::newCrew(int type,int id,double age,string desc,string gender,string name){

	CrewMember* c;
	int index;
	if(age <0 || (index = searchCrewMember(id) != -1)){
		cout<<"Failure"<<endl;
		return;
	}
	switch(type){
	case 0: {c=new Director; break;}
	case 1: {c=new Actor; break;}
	case 2: {c=new Writer; break;}
	case 3: {c=new Producer; break;}
	default: {cout<<"Failure"<<endl; return;}
	}

	c->setId(id);
	c->setAge(age);
	c->setJob(desc);
	c->setGender(gender);
	c->setName(name);
	crews.push_back(c);
	cout<<"Success"<<endl;


}
/***********************************************************
 * gets movie's code and crewmember's id, checks if they exist
 * if both exist, calls to movie's function to add a crew member
 * and to crew member's function to add movie
************************************************************/
void System::addCrewToMovie(string code,int id){
	int movieIndex;
	movieIndex=searchMovie(code);
	id =searchCrewMember(id);
	if(id == -1 || movieIndex == -1){
		cout<<"Failure"<<endl;
		return;
	}
	movies[movieIndex]->addCrew(crews[id]);
	crews[id]->addMeToMovie(movies[movieIndex]);
	cout<<"Success"<<endl;

}
/***********************************************************
 * gets movie's code and a genre
 * if genre already exists in system's db , calls to
 * genre's function to add this movie to its db
 * and to movie's function to add this gnere to its db
 * if genre does not exist, creates new genre and then
 * calls movie and genre function to add
 * if menu = true meaning we came to this function from
 * menu and want to print success , otherwise we came from another
 * function and dont want to print success
************************************************************/
void System::addGenre(string code,string g){
	int movieId = searchMovie(code);
	if(movieId == -1){
		cout<<"Failure"<<endl;
		return;
	}
	int index = searchGenre(g);
	if(index != -10){
		genres[index].addMeToMovie(movies[movieId]);
		movies[movieId]->addGenre(genres[index]);
	}
	else {
		if(index == -10){
			Genre genre;
			genre.setGenre(g);
			genre.addMeToMovie(movies[movieId]);
			genres.push_back(genre);
			movies[movieId]->addGenre(genre);
		}
	}
	if(menu){
	cout<<"Success"<<endl;
	}

}
/***********************************************************
 * checks if sortType is valid. searches movie's index
 * sets movie sortType to the input sortType
 * sorts by sort Type and enters sorted list into movie
************************************************************/
void System::setSortType(string movieId, int isortType){
	byId id;
	byAge age;
	byAmountOfMovies amount;
	if(isortType != 1 && isortType != 2 && isortType != 3){
		cout<<"Failure"<<endl;
		return;
	}
	int index = searchMovie(movieId);
	if(index == -1){
		cout<<"Failure"<<endl;
		return;
	}
	movies[index]->setSort(isortType);
	vector<CrewMember*> c = movies[index]->getCrewList();
		switch(isortType){
		case 1: {sort(c.begin(), c.end(),id); break;}
		case 2: {sort(c.begin(), c.end(),age); break;}
		case 3: {sort(c.begin(), c.end(),amount);break;}
		}
		movies[index]->sort(c);
		if(print){
			cout<<"Success"<<endl;
		}

}
/***********************************************************
 * gets movie's code, checks if code is valid
 * if valid calls to sort before print, while if a sortType
 * was not set by user, the default is type 1 and prints movie
************************************************************/
void System::printMovie(string id){
	int movie;


	movie=searchMovie(id);
	if(movie == -1){
		cout <<"Failure"<<endl;
		return;
	}

	int s = movies[movie]->getSort();
	print = false;
	setSortType(id,s);
	print = true;
	movies[movie]->printMovie();
}
/***********************************************************
 * breaks moviesId by taking each code, break it until we get
 * to char "," and save the code in a Movie* vector. bt the end of the
 * loop we get a vector of Movie* objects with all the movie
 * codes we want to combine.
 * in the loop we search for the max length of the movies we recieved
 * at the end of the loop we also get the index of the movie
 * with max length, and we take its info , create new movie
 * and sets the longest movie info to our new movie
 * then we go through our vector that we created of the movies
 * and send our new movie with every single movie on the vector
 * to the function combineMovie in Movie class and combine
 * genres  and crewmembers.
************************************************************/
void System::combineMovies(string moviesIds){
	int fun, index;
	double max =0;
	stringstream inp(moviesIds);
	string tok, newId;
	vector<Movie*> m;
	bool first = true;
	while(getline(inp,tok,',')){
		if(!first){
			newId.append("_");

		}
		first = false;
		newId.append(tok);
		fun = searchMovie(tok);
		if(fun == -1){
			cout<<"Failure"<<endl;
			return;
		}
		m.push_back(movies[fun]);
		if(movies[fun]->getLength() > max){
			max = movies[fun]->getLength();
			index = fun;
		}
	}
	//movies[index] holds the longest movie
	newMovie(newId,movies[index]->getName(),movies[index]->getLength(),movies[index]->getDate(),movies[index]->getRank(),movies[index]->getSummary());
	index =searchMovie(newId);
	for(unsigned int i=0; i<m.size();i++){
		movies[index]->combineMovies(m[i]);
	}
	vector<CrewMember*> c = movies[index]->getCrewList();
	for(unsigned int i=0;i<c.size();i++){
		c[i]->addMeToMovie(movies[index]);
	}
	menu = false;
	vector<Genre> g =movies[index]->returnGenre();
	for(unsigned int i =0; i<g.size();i++){
		addGenre(newId,g[i].getGenre());
	}


}
/***********************************************************
 * find crew index in systems db
 * checks if valid
 * calls for print his movies with his class function
************************************************************/
void System::printCrewMovies(int code){
	code =searchCrewMember(code);
	if(code == -1){
		cout<<"Failure"<<endl;
		return;
	}
	crews[code]->printAllMovies();
}
/***********************************************************
 * finds movie's index in systems db
 * checks if code is valid
 * calls for printing its crew members in Movie class
************************************************************/
void System::printMoviesCrew(string id){
	int index;
	index = searchMovie(id);
	if(index == -1){
		cout<<"Failure"<<endl;
		return;
	}
	movies[index]->printCrew();

}
/***********************************************************
 * finds movie's index in systems db
 * prints its genres with function from Movie class
************************************************************/
void System::printMoviesGenres(string id){
	int idex =searchMovie(id);
	if(idex == -1){
		cout<<"Failure"<<endl;
		return;
	}
	movies[idex]->getGenre();
}
/***********************************************************
 * finds movie's index in system db
 * checks if code is valid
 * goes through crewmembers db and deletes the movie from their db
 * if exists
 * goes through genres db and deletes the movie from their db
 * if exists
 * at the end deletes movie from system's db
************************************************************/
void System::deleteMovie(string code){

int index =0;
index = searchMovie(code);
if(index == -1){
	cout<<"Failure"<<endl;
	return;
}
		//delete movie from every crew member
		for(unsigned int i=0;i<crews.size();i++){
			if(crews[i]->searchMovie(code)){
				crews[i]->deleteMeFromMovie(code);
			}

		}
		for(unsigned int i=0;i<genres.size();i++){
			genres[i].deleteMovie(code);
		}

		movies.erase(movies.begin() + index);
		cout<<"Success"<<endl;

}
/***********************************************************
 * find crew's index in systems db
 * checks if valid
 * goes through movies db, if crew exists in a movie
 * delets crew from the movie's db
 * at the end deletes crew from systems db
************************************************************/
void System::deleteCrew(int id){
			int tmpCode=id;
			id = searchCrewMember(id);
			if(id == -1){
				cout<<"Failure"<<endl;
				return;
			}
			for(unsigned int i=0;i<movies.size();i++){
					if(movies[i]->searchCrew(tmpCode)){
						movies[i]->deleteCrew(crews[id]);
					}
				}
			crews.erase(crews.begin() + id);
			cout<<"Success"<<endl;
}
/***********************************************************
 * finds crew's index a nd movie's index in systems db
 * deletes crew from the movie's db
************************************************************/
void System::deleteCrewFromMovie(string movieCode, int crewCode){
	int index;
	crewCode = searchCrewMember(crewCode);
	index = searchMovie(movieCode);
	if(crewCode == -1 || index == -1){
		cout<<"Failure"<<endl;
		return;
	}
	movies[index]->deleteCrew(crews[crewCode]);
	cout<<"Success"<<endl;


}
/***********************************************************
 * goes through movies db, if the input id was found returns
 * the index, else returns -1.
************************************************************/
int System::searchMovie(string movieId){
	for( unsigned int i = 0; i < movies.size(); ++i){
			if ((movies[i]->getCode().compare(movieId))==0)
				return i;
		}
	return -1;
}
/***********************************************************
 * goes through crewmembers db , if input id was found,
 * returns index, else returns -1
************************************************************/
int System::searchCrewMember(int crewId){
	for(unsigned int i = 0; i != crews.size(); ++i){
				if (crews[i]->getId() == crewId)
					return i;
			}
		return -1;
}
/***********************************************************
 * goes through movies db and prints every movie's info
************************************************************/
void System::printAllMovies(){

	for( unsigned int i = 0; i != movies.size(); ++i){
		movies[i]->printMovie();

	}

}
/***********************************************************
 * goes through crewmembers db and prints every crew member info
************************************************************/
void System::printAllCrews(){
	for( int i = 0; i < crews.size(); i++){
			crews[i]->printMember();
			cout << endl;
		}
}
/***********************************************************
 * goes through genre db and returns index if exists in db
 * else returns -10
************************************************************/
int System::searchGenre(string g){

	for(int i=0; i<genres.size(); i++){
		if(genres[i].getGenre() == g)
			return i;
	}
	return -10;
}
/***********************************************************
 * gets genre input, if genre exists in db calls to print
 * movies that genre was added to
************************************************************/
void System::printByGenre(string input){
	int index;
	index = searchGenre(input);
	if(index != -10)
	genres[index].printMyMovies();
	else
		cout<<"No movies in this genre";

}
/***********************************************************
 * at the end of program when input code is -1
 * deletes all the data bases and free memory
************************************************************/
void System::terminate(){

}

