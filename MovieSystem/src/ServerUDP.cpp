/************************************************
* Student Name: Anna Bloch and Rotem Elem		*
* Exercise Name: Ex 4							*
* File description: server UDP class    		*
*************************************************/
#include "ServerUDP.h"

/***********************************************************
 * constructor, receives port
************************************************************/
ServerUDP::ServerUDP(int uport){
	port = uport;
}
/***********************************************************
 * deconstructor
************************************************************/
ServerUDP::~ServerUDP(){}
/***********************************************************
 * initialize udp connection with client, creates sock
************************************************************/
void ServerUDP::initialization(){
	  sock = socket(AF_INET, SOCK_DGRAM, 0);
	  if (sock < 0) {
	      perror("error creating socket");
  }
	 // struct sockaddr_in sin;
	  memset(&sin, 0, sizeof(sin));

	  sin.sin_family = AF_INET;
	  sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons(port);

 	    if (bind(sock, (struct sockaddr *) &sin, sizeof(sin)) < 0) {
	            perror("error binding to socket");
	     }
}
/***********************************************************
 * reads bytes from client, recived bytes are saved in a
 * string form and are returned to the program
************************************************************/
string ServerUDP::readFromClient(){
	 unsigned int from_len = sizeof(struct sockaddr_in);
		 char buffer[4096];
		 bytes = recvfrom(sock, buffer, sizeof(buffer), 0, (struct sockaddr *) &from, &from_len);
	  	 if (bytes < 0) {
		     perror("error reading from client's socket");
		 }
	  	string buff(buffer);
	    return buff;
}
/***********************************************************
 * getting string as input, in our case, the output from
 * the client's input line, and sends output back to client
************************************************************/
void ServerUDP::sendToClient(const string& line){
	char buffer[4096];
	strcpy(buffer,line.c_str());
		buffer[line.size()]= 0;
		int sent_bytes = sendto(sock, buffer, bytes, 0, (struct sockaddr *) &from, sizeof(from));
		    if (sent_bytes < 0) {
		        perror("error writing to client's socket");
		    }
}
/***********************************************************
 * close connection
************************************************************/
void ServerUDP::closeConnection(){
	close(sock);
}
