/************************************************
* Student Name: Anna Bloch and Rotem Elem		*
* Exercise Name: Ex 4							*
* File description: server TCP class    		*
*************************************************/
#include "ServerTCP.h"

using namespace std;

/***********************************************************
 * constructor, receives port
************************************************************/
ServerTCP::ServerTCP(int uport){
	port = uport;
}
ServerTCP::ServerTCP(const ServerTCP* s){
	sock = s->sock;
	port = s->port;
	client_sock = s->client_sock;
}
/***********************************************************
 * deconstructor
************************************************************/
ServerTCP::~ServerTCP(){}
/***********************************************************
 * initialize tcp connection with client, creates sock
************************************************************/
void ServerTCP::initialization(){

	sock = socket(AF_INET, SOCK_STREAM, 0);
	    if (sock < 0) {
	        perror("error creating socket");
	    }

	    struct sockaddr_in sin;
	    memset(&sin, 0, sizeof(sin));
	    sin.sin_family = AF_INET;
	    sin.sin_addr.s_addr = INADDR_ANY;
	    sin.sin_port = htons(port);

	    if (bind(sock, (struct sockaddr *) &sin, sizeof(sin)) < 0) {
	        perror("error binding socket");
	    }

	    if (listen(sock, 5) < 0) {
	        perror("error listening to a socket");
	    }

	    struct sockaddr_in client_sin;
	    unsigned int addr_len = sizeof(client_sin);
	    client_sock = accept(sock,  (struct sockaddr *) &client_sin,  &addr_len);

	    if (client_sock < 0) {
	        perror("error accepting client");
	    }
}
/***********************************************************
 * reads bytes from client, recived bytes are saved in a
 * string form and are returned to the program
************************************************************/
string ServerTCP::readFromClient(){

	        char buffer[4096];
	            int expected_data_len = sizeof(buffer);
	            read_bytes = recv(client_sock, buffer, expected_data_len, 0);
	            if (read_bytes == 0) {
	            // connection is closed
	            }
	            else if (read_bytes < 0) {
	            // error
	            }
	            else {
	                string line(buffer);
	            	return line;
	            }

}
/***********************************************************
 * getting string as input, in our case, the output from
 * the client's input line, and sends output back to client
************************************************************/
void ServerTCP::sendToClient(const string& line){
	char buffer[4096];
	strcpy(buffer,line.c_str()); // check last note so it wont print zero
		buffer[line.size()]= 0;
		int sent_bytes = send(client_sock, buffer, line.size() +1 , 0);
		    if (sent_bytes < 0) {
		        perror("error sending to client");
		    }

}
/************************************************************
*creating socket for each client's thread and reading data							*
*************************************************************/
ServerTCP* ServerTCP::communicate(){
	ServerTCP* con = new ServerTCP(this);
	struct sockaddr_in client_sin;
	unsigned int addr_len = sizeof(client_sin);
	con->client_sock = accept(sock,  (struct sockaddr *) &client_sin,  &addr_len);
		  if (con->client_sock < 0) {
		        perror("error accepting client");
		    }

		    return con;
}
/***********************************************************
 * close connection
************************************************************/
void ServerTCP::closeConnection(){
	close(sock);
}

