/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: all of the crew members type extend
* crew member. all of the functions are implemented
* in crew member except the print function.
****************************************/

#include <list>
#include <string>
#include <iostream>
#include "CrewMember.h"
using namespace std;

CrewMember::CrewMember(){} //constructor
CrewMember::~CrewMember(){}

void CrewMember::setName(string name){
	myName =name;
}
string CrewMember::getName(){ return myName;}
void CrewMember::setId(int id){
	myId =id;
}
int CrewMember::getId() const{return myId;}
void CrewMember::setGender(string g){
	myGender =g;
}
string CrewMember::getGender(){return myGender;}
void CrewMember::setJob(string job){
	myJob =job;
}
string CrewMember::getJob(){return myJob;}
void CrewMember::setAge(double age){
	myAge = age;
}
double CrewMember::getAge()const{return myAge;}
/***********************************************************
 * recieves movie m and adds it to crew member's db
************************************************************/
void CrewMember::addMeToMovie(Movie* m){
	movies.push_back(m);
}
/***********************************************************
 * function only implemented in sons
************************************************************/
void CrewMember::printMember(){}
/***********************************************************
 * goes over entire movies data base of crew member
 * and prints one by one the movie's info
************************************************************/
void CrewMember::printAllMovies(){
	for( int i = 0; i < movies.size(); ++i){
			movies[i]->printMovie();
		}
}
void CrewMember::deleteCrewMember(){} //deletes itself
/***********************************************************
 * goes through crew member's movie db, searches for
 * movie id from input , if finds returns index, else returns -1
 * only if finds input index, removes movie from my db
************************************************************/
void CrewMember::deleteMeFromMovie(string id){
	int index = -1;
	for(int i=0; i<movies.size(); i++){
			if ((movies[i]->getCode()).compare(id) == 0){
				index =i;
			}
		}
	if(index != -1){
	movies.erase(movies.begin() + index);
	}
}
/***********************************************************
 * goes through crew movies db, if finds input id - returns
 * true - movie exists in crew member's db, else returns false
************************************************************/
bool CrewMember::searchMovie(string id){
	for(int i=0; i<movies.size(); i++){
		if (movies[i]->getCode() == id){
			return true;
		}
	}
	return false;
}
/***********************************************************
 * returns crew's movies db size, which indicates
 * in how many movies crew has participated
************************************************************/
int CrewMember::amountOfMovies()const{ return movies.size();}






