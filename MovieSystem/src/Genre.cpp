/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Genre class will hold all
* the possible types of Genres
****************************************/
#include <list>
#include <string>
#include <iostream>
#include "Genre.h"
using namespace std;




Genre::Genre(string genre){
	//constructor

}
void Genre::setGenre(string g){
	type =g;
}
string Genre::getGenre(){ return type;}
Genre::Genre(){}
Genre::~Genre(){}
/***********************************************************
 * goes thorugh genre movies db and prints their info
************************************************************/
void Genre::printMyMovies(){
	for(int i=0; i<movies.size(); i++){
		movies[i]->printMovie();
	}
}
/***********************************************************
 * goes through genre movies db and searches if a movie
 * with the id we got via input exists in genre's db, if exists
 * returns its index, else returns -1
************************************************************/
int Genre::searchMovie(string id){

	for(int i=0; i<movies.size(); i++){
		if((movies[i]->getCode()).compare(id) == 0)
			return i;
	}
	return -1;
}
/***********************************************************
 * pushes movie object in genre's movies db
************************************************************/
void Genre::addMeToMovie(Movie* m){

	movies.push_back(m);

}
/***********************************************************
 * gets movie's index by sending its input code
 * to searchMovie function, and delete it from db
************************************************************/
void Genre::deleteMovie(string code){
	int index = searchMovie(code);
	if(index != -1){
	movies.erase(movies.begin() + index);
	}
}
