/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Writer object derives from CrewMember
* all of his functions except the print function.
****************************************/
#include "Writer.h"
#include <iostream>
using namespace std;


Writer::Writer(){}
Writer::~Writer(){}
/***********************************************************
 * print writer's info
************************************************************/
void Writer::printMember(){
	cout<<myName<<" "<<myJob;
}

