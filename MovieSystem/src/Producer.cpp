/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Producer is a type of CrewMember
* we can create a new object or print its info
****************************************/
#include "Producer.h"
#include <iostream>
using namespace std;

Producer::Producer(){} //constructor
Producer::~Producer(){}
/***********************************************************
 * prints producers info
************************************************************/
void Producer::printMember(){
	cout<<myName;
}

