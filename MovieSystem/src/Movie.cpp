/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Movie holds CrewMember and Genre database
* and manages the movie object data bases , prints info and create new
* Movie objects.
****************************************/

#include <list>
#include <string>
#include <iostream>
#include <algorithm>
#include "Movie.h"
#include "Genre.h"
#include "CrewMember.h"

using namespace std;



Movie::Movie(){
	srtType =1;
}
Movie::~Movie(){}
void Movie::setName(string name){
	myName = name;
}
string Movie::getName(){
	return myName;
}
void Movie::setDate(int date){
	myYear = date;
}
int Movie::getDate(){return myYear;}

void Movie::getGenre(){
	for(int i=0; i<genre.size(); i++)
	{
		cout<<genre[i].getGenre()<<" ";
	}
	cout<<endl;
}
/***********************************************************
 * returns genre vector
************************************************************/
vector<Genre> Movie::returnGenre(){
	return genre;
}

void Movie::setCode(string code){
	myCode = code;
}
string Movie::getCode(){return myCode;}
void Movie::setLength(double length){
	myLength=length;
}
double Movie::getLength(){return myLength;}
void Movie::setRank(double rank){
	myRank=rank;
}
double Movie::getRank(){return myRank;}
void Movie::setSummary(string summary){
	mySummary=summary;
}
/***********************************************************
 * set sort type while the defualt sort type is 1
************************************************************/
void Movie::setSort(int sort){
	srtType = sort;
}
int Movie::getSort(){
	return srtType;
}
string Movie::getSummary(){return mySummary;}

/***********************************************************
 * adds crwmember c into crewmembers db, returns true if success
************************************************************/
bool Movie::addCrew(CrewMember* c){
	if(!searchCrew(c->getId())){
	crewmembers.push_back(c);
	}
	return true;
}
/***********************************************************
 * returns crew members vector
************************************************************/
vector<CrewMember*> Movie::getCrewList(){
	return crewmembers;
}

/***********************************************************
 * calls to searchGenre, if genre does not exist, adds to genre
 * db. returns true if successful
************************************************************/
bool Movie::addGenre (Genre g){
	if(!searchGenre(g.getGenre())){
		genre.push_back(g);
	}
	return true;}
/***********************************************************
 * goes through genre db , if input genre exists in db
 * returns true
************************************************************/
bool Movie::searchGenre(string g){
	for(int i =0;i<genre.size();i++){
		if(genre[i].getGenre() == g)
			return true;
	}
	return false;
}
/***********************************************************
 * recieves sorted crewmembers list and put it
 * as our new crew member list
************************************************************/
void Movie::sort(vector<CrewMember*> c){

	crewmembers = c;

}
/***********************************************************
 * goes through crew members db and prints every
 * crew member info
************************************************************/
void Movie::printCrew(){

	for( int i = 0; i < crewmembers.size(); ++i){
		crewmembers[i]->printMember();
		cout<<endl;

	}
}
/***********************************************************
 * prints movie info and its crewmembers vector
************************************************************/
void Movie::printMovie(){
	cout<<myCode<<" "<<myName<<" "<<myLength<<" "<<myYear<<" "<<myRank<<" "<<mySummary<<endl;
	printCrew();
}
/***********************************************************
 * recieves another movie object to combine info with our info.
 * saves its genre list and crew members list and adds other movie
 * genres and crew member to our data base if they dont already exist
************************************************************/
void Movie::combineMovies( Movie* m){
	vector<Genre>other = m->returnGenre();
	vector<CrewMember*> otherC =m->getCrewList();
	for(unsigned int i=0;i<other.size();i++){
		addGenre(other[i]);
	}
	for(unsigned int i=0;i<otherC.size();i++){
		addCrew(otherC[i]);
	}
	}
void Movie::deleteMe(){} //deletes itself
/***********************************************************
 * recieves crew member object, searches if crew member exists
 * in this movie's crew members, if exists, deletes it from
 * crew members data base
************************************************************/
void Movie::deleteCrew(CrewMember* c){
	int index=-1;
	for(int i=0; i<crewmembers.size();i++){
		if(crewmembers[i]->getId() == c->getId())
			index = i;
	}
	if(index != -1){
		crewmembers.erase(crewmembers.begin() + index);
	}
	else
		cout<< "this Crew member was not found in this movie";
}
/***********************************************************
 * goes through crew members db , if id that was recieved
 * through input exists in db, returns true
************************************************************/
bool Movie::searchCrew(int id){
	for(int i=0; i<crewmembers.size();i++){
		if(crewmembers[i]->getId() == id)
			return true;
	}
	return false;
}



