/****************************************
* Student Name:Rotem Elem
* Exercise Name: Targil 2
* File description: Actor is a type of CrewMember,
* we can create a new Actor object or print its info.
****************************************/
#include "Actor.h"
#include <iostream>
using namespace std;

Actor::Actor(){} //constructor
Actor::~Actor(){};
/***********************************************************
 * prints member info
************************************************************/
void Actor::printMember(){
	cout<<myName<<" "<<myAge;
}
