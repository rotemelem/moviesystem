/* 8921003 204504740 Rotem Elem
 * 8921003 312936438 Anna Bloch*/
#include <stdio.h>
#include <stdlib.h>
#include "Menu.h"
#include "ServerTCP.h"
#include "ServerUDP.h"
#include <pthread.h>

using namespace std;

/**********************************************
 * handles single connection each time *
***********************************************/
void *singleConnection(void *c){
ServerConnection* co = (ServerConnection*) c;
Menu m;
System* s = System::getInstance();
	m.start(co,s);
	co->closeConnection();
	delete co;
	}


int main(int argc, char **argv) {
/*********************************************************************
* Program name: Main
* The operation: Runs program, gets arguments from argv,
* argv[1] is connection type, argv[2] is port number,
* creates connection by type and then runs program.
* via Menu class, gets client's input line , and execute functions.
* the output that functions return or print, is sent back to client.
* when client sends -1 operation stops.
*********************************************************************/

	int type = atoi(argv[1]);
	int port = atoi(argv[2]);
	ServerTCP* c;
	if(type == 0){
		cout<<"Can only communicate via TCP protocole"<<endl;

	}
	if(type == 1){
		c = new ServerTCP(port);
		c->initialization();
		System::openLock();
		while(1){
			pthread_t t1;
			ServerTCP* tCon = c->communicate();
			int status = pthread_create(&t1,NULL,singleConnection,(void*)tCon);
			if (status != 0){
							cout<<"error creating thread";
						}
		}
	}

	delete c;

}
