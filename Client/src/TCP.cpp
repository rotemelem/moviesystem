/************************************************
* Student Name: Anna Bloch and Rotem Elem		*
* Exercise Name: Ex 4							*
* File description: client TCP class    		*
*************************************************/
#include "TCP.h"
#include "Connection.h"
using namespace std;


/***********************************************************************
 * constructor														   *
 ***********************************************************************/
TCP::TCP(int port_no,char* ip_address):Connection(port_no,ip_address){}
/***********************************************************************
 * deconstructor														   *
 ***********************************************************************/
TCP::~TCP(){}
/***********************************************************************
 * creates a new socket, connects with the server														   *
 ***********************************************************************/
void TCP::creation(){
sock = socket(AF_INET, SOCK_STREAM, 0);
if (sock < 0) {
    perror("error creating socket");
}

struct sockaddr_in sin;
memset(&sin, 0, sizeof(sin));
sin.sin_family = AF_INET;
sin.sin_addr.s_addr = inet_addr(ip_address);
sin.sin_port = htons(port_no);
if (connect(sock, (struct sockaddr *) &sin, sizeof(sin)) < 0) {
        perror("error connecting to server");
    }
}
/***********************************************************************
 * gets a string, converts it to char[]
 * and then sends it to the server														   *
 ***********************************************************************/
void TCP::sendServer(string& input){

		char data_addr[4096];
		strcpy(data_addr,input.c_str());
	    int data_len = strlen(data_addr);
	    int sent_bytes = send(sock, data_addr, data_len, 0);

	    if (sent_bytes < 0) {
	    	perror("error sending to server");
	    }
}
/***********************************************************************
 * gets data from server and returns the data to the main class												   *
 ***********************************************************************/
string TCP::readFromServer(){
	    char buffer[4096];

	    int expected_data_len = sizeof(buffer);
	    int read_bytes = recv(sock, buffer, expected_data_len, 0);
	    if (read_bytes == 0) {
	    	perror("connection is closed");
	    }
	    else if (read_bytes < 0) {
	    	perror("connection is closed");
	    }
	    else {

	         return buffer;
	    }
}
/***********************************************************************
 * close the connection											   *
 ***********************************************************************/
void TCP::closeConnection(){
	    close(sock);
}

