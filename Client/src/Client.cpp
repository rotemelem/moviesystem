/* 8921003 312936438 Anna Bloch
 * 8921003 204504740 Rotem Elem   */

#include "TCP.h"
#include "UDP.h"
#include "Connection.h"
#include <sstream>
#include <string>
#include <iostream>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>

using namespace std;
/*********************************************************************
 * Program name:EX 4
 * The operation: main client class
 * argv - gets the type(TCP or UDP), ip address and port number,
 * the program runs untill user enters -1 as code,
 * then the loop stops(using boolean argument).
 *  it creates a new connection, gets the input and sends to server.
 * then receives the output and prints it.
 *********************************************************************/
int main(int argc,char **argv){

	int type= atoi(argv[1]);
	char* ip_address=argv[2];
	int port_no=atoi(argv[3]);



	Connection* con;

	string line;
	string code ="0";
	bool stop = false;

		if(type==1){
			con=new TCP(port_no,ip_address);
		}
		if(type==0){
			con=new UDP(port_no,ip_address);

		}
		con->creation();
		while(!stop){
			getline(cin,line);
			string m(line);
			con->sendServer(m);
			if(m.substr(0,2) == "-1")
			{ stop = true;}
			else{
			string output=con->readFromServer();
			cout<<output;}
		}

	con->closeConnection();
	delete con;


}

