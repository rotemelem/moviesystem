/************************************************
* Student Name: Anna Bloch and Rotem Elem		*
* Exercise Name: Ex 4							*
* File description: client UDP class    		*
*************************************************/
#include "Connection.h"
#include "UDP.h"
/***********************************************************************
 * constructor														   *
 ***********************************************************************/
 UDP::UDP(int port_no, char* ip_address) : Connection(port_no, ip_address) {


}
/***********************************************************************
 * distructor														   *
 ***********************************************************************/
UDP::~UDP() {

}
/***********************************************************************
 * creates a new socket, connects with the server														   *
 ***********************************************************************/
void UDP::creation(){

	  sock = socket(AF_INET, SOCK_DGRAM, 0);
	  if (sock < 0) {
	      perror("error creating socket");
    }
	 // struct sockaddr_in sin;
	  memset(&sin, 0, sizeof(sin));

	  sin.sin_family = AF_INET;
	  sin.sin_addr.s_addr = inet_addr(ip_address);
      sin.sin_port = htons(port_no);

}
/***********************************************************************
 * reads data from server and returns the data												   *
 ***********************************************************************/
string UDP::readFromServer(){

   // struct sockaddr_in from;
    unsigned int from_len = sizeof(struct sockaddr_in);
    char buffer[4096];
    memset(&buffer, 0, sizeof(buffer));
    int bytes = recvfrom(sock, buffer, sizeof(buffer), 0, (struct sockaddr *) &from, &from_len);
    if (bytes < 0) {
        perror("error reading from socket");
    }
    if (bytes == 0) {
         perror("empty message recieved");
     }

    return buffer;
}
/***********************************************************************
 * gets a string, converts it to char[]
 * and then sends it to the server														   *
 ***********************************************************************/
void UDP::sendServer(string& buffer){
	char data_addr[4096];

	strcpy(data_addr,buffer.c_str()); // check last note so it wont print zero
	int data_len = sizeof(data_addr);
	int sent_bytes = sendto(sock, data_addr, data_len, 0, (struct sockaddr *) &sin, sizeof(sin));
	    if (sent_bytes < 0) {
	        perror("error writing to socket");
	    }

}


/***********************************************************************
 * close connection of the socket														   *
 ***********************************************************************/
void UDP::closeConnection(){

	close(sock);
}
