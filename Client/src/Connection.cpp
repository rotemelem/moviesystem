/************************************************
* Student Name: Anna Bloch and Rotem Elem		*
* Exercise Name: Ex 4							*
* File description: client connection class    	*
*************************************************/
#include "Connection.h"
using namespace std;

/***********************************************************************
* constructor											   *
***********************************************************************/
Connection::Connection(int myPort_no,char* myIp_address){
	ip_address = myIp_address;
	port_no = myPort_no;
}
/***********************************************************************
* distructor											   *
***********************************************************************/
Connection::~Connection(){}

