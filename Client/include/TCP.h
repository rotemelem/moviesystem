/************************************************
* Student Name: Anna Bloch and Rotem Elem		*
* Exercise Name: Ex 4							*
* File description: client TCP header    		*
*************************************************/
#ifndef TCP_H_
#define TCP_H_
#include <iostream>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include "Connection.h"

using namespace std;

class TCP : public Connection {
public:
	/***********************************************************************
		* constructor														   *
		***********************************************************************/
		TCP(int myPort_no,char* myIp_address);
		/***********************************************************************
		* deconstructor														   *
		***********************************************************************/
		virtual ~TCP();
		/***********************************************************************
		* create a connection													*
		***********************************************************************/
		void creation();
		/***********************************************************************
		* gets data from server													   *
		***********************************************************************/
		string readFromServer();
		/***********************************************************************
		* send data to server													   *
		***********************************************************************/
		void sendServer(string& input);
		/***********************************************************************
		* close connection												   *
		***********************************************************************/
		void closeConnection();



};


#endif /* TCP_H_ */
