/************************************************
* Student Name: Anna Bloch and Rotem Elem		*
* Exercise Name: Ex 4							*
* File description: client UDP header    		 *
*************************************************/
#ifndef UDP_H_
#define UDP_H_
#include <iostream>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#include "Connection.h"

class UDP : public Connection{
public:
	/***********************************************************************
	 * constructor														   *
	 ***********************************************************************/
	UDP(int port_no,  char* ip_address);
	/***********************************************************************
	 * distructor														   *
	 ***********************************************************************/
	virtual ~UDP();
	/***********************************************************************
	 * create connection														   *
	 ***********************************************************************/
	void creation();
	/***********************************************************************
	 * gets data from server														   *
	 ***********************************************************************/
	string readFromServer();
	/***********************************************************************
	 * send data to server													   *
	 ***********************************************************************/
	void sendServer(string& buffer);
	/***********************************************************************
	 * close connection														   *
	 ***********************************************************************/
    void closeConnection();
private:
    struct sockaddr_in sin;
    struct sockaddr_in from;
    int bytes;
};

#endif /* UDP_H */
