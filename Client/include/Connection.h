/************************************************
* Student Name: Rotem Elem, Anna Bloch	*
* Exercise Name: Ex 4							*
* File description: connection of client, header    *
*************************************************/
#ifndef CONNECTION_H_
#define CONNECTION_H_
#include <iostream>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

using namespace std;
class Connection {
public:
	char* ip_address;
	int port_no;
	int type;
	int sock;

	/***********************************************************************
	 * constructor														   *
	 ***********************************************************************/
	Connection(int port_no,char* ip_address);
	/***********************************************************************
	 * distructor														   *
	 ***********************************************************************/
	virtual ~Connection();
	/***********************************************************************
	 * create connection														   *
	 ***********************************************************************/
	void virtual creation()=0;
	/***********************************************************************
	 * read date from server														   *
	 ***********************************************************************/
	string virtual readFromServer()=0;
	/***********************************************************************
	 * send data to server														   *
	 ***********************************************************************/
	void virtual sendServer(string& output)=0;
	/***********************************************************************
	 * close connection														   *
	 ***********************************************************************/
	void virtual closeConnection()=0;

};

#endif /* CONECTTION_H_ */
